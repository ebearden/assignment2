/**
 * Iterator for the AlbumList.
 * @author Elvin Bearden
 * @version Created on: Feb 3, 2013
 */
import java.util.Iterator;

public class AlbumListIterator implements Iterator<Album> {
	private AlbumList alist;
	private int current;
	
	/**
	 * Creates a new Iterator for the AlbumList.
	 * @param alist		The AlbumList to be Iterated.
	 */
	public AlbumListIterator(AlbumList alist) {
		this.alist = alist;
		current = 0;
	}
	
	/**
	 * Tells the iterator if it should continue.
	 * @return true if current < alist.count else false
	 */
	public boolean hasNext() {
		return (current < alist.count());
	}
	
	/**
	 * Returns the next item in the list. 
	 * @returns		Next item.
	 */
	public Album next() {
		return alist.getElementAt(current++);
		
	}
	
	/**
	 * Not implemented.
	 */
	public void remove() {
		throw new RuntimeException("Remove not supported.");
	}
}
