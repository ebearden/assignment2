import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * User Interface Class View.
 * 
 * @author Elvin Bearden
 * @version Created on: Jan 18, 2013	
 * @version Modified on: 2/9/13: 
 * 			Added the sort button functionality and refreshDisplay() method.
 */
public class View extends JFrame {
	// Information Entry 
	private JPanel infoEntryPanel;
	private JTextField artistTextField;
	private JTextField albumNameTextField;
	private JTextField yearTextField;
	private JTextField trackCountTextField;
	private JComboBox<String> formatComboBox;
	private String[] formatChoiceList = {"CD", "Record", "MP3", "Tape"};
	
	private JPanel btnPanel;
	private JButton addBtn;
	private JButton saveBtn;
	private JButton removeBtn;
	
	// Search 
	private JPanel searchPanel;
	private JTextField searchTextField;
	private JButton searchBtn;
	
	// Display
	private JPanel displayPanel;
	private TextArea displayTextArea;
	private JButton sortBtn;
	
	// Data Structure
	private AlbumList alist;
	
	/** 
	 * View Class:
	 * Interacts with user and keeps track of the AlbumList.
	 * 
	 */
	public View() {
		setLayout(new BorderLayout());
		
		// Information Entry Panel
		infoEntryPanel = new JPanel(new GridLayout(7,1));
		
		JLabel artistLbl = new JLabel("Artist:");
		artistTextField = new JTextField(20);
		
		JLabel albumNameLbl = new JLabel("Album Name:");
		albumNameTextField = new JTextField(20);
		
		JLabel yearLbl = new JLabel("Year:");
		yearTextField = new JTextField(20);
		
		JLabel trackCountLbl = new JLabel("Track Count:");
		trackCountTextField = new JTextField(20);
		
		JLabel formatLbl = new JLabel("Format:");
		formatComboBox = new JComboBox<String>(formatChoiceList);
		
		btnPanel = new JPanel(new BorderLayout());
		addBtn = new JButton("Add");
		removeBtn = new JButton("Remove");
		saveBtn = new JButton("Save");
		
		// Add Entry Components
		infoEntryPanel.add(artistLbl);
		infoEntryPanel.add(artistTextField);
		infoEntryPanel.add(albumNameLbl);
		infoEntryPanel.add(albumNameTextField);
		infoEntryPanel.add(yearLbl);
		infoEntryPanel.add(yearTextField);
		infoEntryPanel.add(trackCountLbl);
		infoEntryPanel.add(trackCountTextField);
		infoEntryPanel.add(formatLbl);
		infoEntryPanel.add(formatComboBox);
		
		// Add Buttons
		btnPanel.add(addBtn, BorderLayout.WEST);
		btnPanel.add(removeBtn, BorderLayout.CENTER);
		btnPanel.add(saveBtn, BorderLayout.EAST);
		removeBtn.setEnabled(false);
		saveBtn.setEnabled(false);
		add(infoEntryPanel, BorderLayout.NORTH);
		infoEntryPanel.add(btnPanel, 10);
		
		// Search Panel
		searchPanel = new JPanel();
		searchTextField = new JTextField(20);
		
		JLabel searchLabel = new JLabel("Search by album name:");
		searchBtn = new JButton("Search");
		
		// Add Search Components
		searchPanel.add(searchLabel);
		searchPanel.add(searchTextField);
		searchPanel.add(searchBtn);
		add(searchPanel, BorderLayout.CENTER);
		
		// Display Panel
		displayPanel = new JPanel(new BorderLayout());
		displayTextArea = new TextArea(11,50);
		displayTextArea.setEditable(false);
		
		// Add Display Components
		displayPanel.add(displayTextArea, BorderLayout.NORTH);
		add(displayPanel, BorderLayout.SOUTH);
		
		// Add Sort Button
		sortBtn = new JButton("Sort");
		displayPanel.add(sortBtn, BorderLayout.CENTER);
		
		// Create Album List and Add Event Listeners
		alist = new AlbumList(50);
		addListeners();
		
		// JFrame Properties
		setSize(415,500);
		setLocation(100,100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	/**
	 * Adds event listeners to buttons.
	 */
	private void addListeners() {
		
		// Add Button
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addItemToList();
				refreshDisplay();
				
			}
		});
		
		// Search Button
		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchForItem();
			}
		});
		
		// Remove Button
		removeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayTextArea.setText(alist.toString());
				clearInfoFields();
				removeBtn.setEnabled(false);
				addBtn.setEnabled(true);
				saveBtn.setEnabled(false);
				refreshDisplay();
			}
		});
		
		// Save Button
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addItemToList();
				clearInfoFields();
				addBtn.setEnabled(true);
				removeBtn.setEnabled(false);
				saveBtn.setEnabled(false);
				refreshDisplay();
			}
		});
		
		sortBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				alist.sort();
				refreshDisplay();
			}
		});
	}
	
	/**
	 * Adds an Album item to the list
	 * and checks that input is valid.
	 */
	private void addItemToList() {
		try {
			alist.add(new Album(artistTextField.getText(), albumNameTextField.getText(), Integer.parseInt(yearTextField.getText()),
						Integer.parseInt(trackCountTextField.getText()), formatComboBox.getSelectedItem().toString()));
			clearInfoFields();
		}
		catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "Year and track count must be integers, all fields must be full");
		}
		catch (IllegalArgumentException ex) {
			JOptionPane.showMessageDialog(this, "Error: List is full");
		}
	}
	
	/**
	 * Clears the fields.
	 */
	private void clearInfoFields() {
		artistTextField.setText("");
		albumNameTextField.setText("");
		yearTextField.setText("");
		trackCountTextField.setText("");
		formatComboBox.setSelectedIndex(0);
	}
	
	/**
	 * Search for an item in the list. 
	 * If found: populate the fields for editing, 
	 * enable remove and save buttons and disable add button. 
	 * If not found show a message.
	 */
	private void searchForItem() {
		try {
			Album found = alist.search(new Album("",searchTextField.getText(), 0, 0, ""));
			
			artistTextField.setText(found.getArtist());
			albumNameTextField.setText(found.getAlbumName());
			yearTextField.setText(Integer.toString(found.getYear()));
			trackCountTextField.setText(Integer.toString(found.getTrackCount()));
			formatComboBox.setSelectedItem(found.getFormat());
			alist.remove(found); //<--Removed now then either user clicks remove and nothing happens
											    //---or re-added once user clicks save.	
			searchTextField.setText("");
			addBtn.setEnabled(false);
			removeBtn.setEnabled(true);
			saveBtn.setEnabled(true);
		}
		catch (NullPointerException ex) {
			JOptionPane.showMessageDialog(this, "Album cannot be found.");
		}
	}
	
	/**
	 * Refreshes the display. Uses the Iterator for the AlbumList 
	 * to make the for(each) loop possible.
	 */
	private void refreshDisplay() {
		String display = "";
		for (Album i : alist) {
			display += i.toString();
		}
		displayTextArea.setText(display);
	}
}
