/**
 * Represents a musical album.
 * 
 * @author Elvin Bearden
 * @version Created on: Jan 18, 2013	
 * @version Modified on: 2/3/13: 
 * 			Added Comparable and Cloneable Interfaces and implementations.
 * 
 * TODO: Fix sort method to also work with integers.
 */
public class Album implements Cloneable, Comparable<Album>{
	private String artist;
	private String albumName;
	private int year;
	private int trackCount;
	private String format;
	
	/**
	 * Constructor for Album
	 * 
	 * @param artist		Name of the artist
	 * @param albumName		Name of the album
	 * @param year			Year album released
	 * @param trackCount	Number of tracks
	 * @param format		Format of the album
	 */
	public Album(String artist, String albumName, int year, int trackCount, String format) {
		this.artist = artist;
		this.albumName = albumName;
		this.year = year;
		this.trackCount = trackCount;
		this.format = format;
	}
	
	/**
	 * 
	 * @return artist 	Name of the artist
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * 
	 * @param artist	Name of the artist
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	/**
	 * 
	 * @return albumName	Name of the album
	 */
	public String getAlbumName() {
		return albumName;
	}

	/**
	 * 
	 * @param albumName		Name of the album
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	/**
	 * 
	 * @return year 	Year album released
	 */
	public int getYear() {
		return year;
	}

	/** 
	 * @precondition 		year > 0
	 * @param year			Year album released
	 * @throws Exception	Throws IllegalArgumentException if year is not greater than 0.
	 */
	public void setYear(int year) throws Exception {
		if (year > 0) {
			this.year = year;
		}
		else {
			throw new IllegalArgumentException("Year must be greater than 0.");
		}
	}

	/**
	 * 
	 * @return trackCount 		Number of tracks
	 */
	public int getTrackCount() {
		return trackCount;
	}

	/**
	 * @precondition		trackCount > 0
	 * @param trackCount 	Number of tracks
	 * @throws Exception	Throws IllegalArgumentException if trackCount is not greater than 0.
	 */
	public void setTrackCount(int trackCount) throws Exception {
		if (trackCount > 0) {
			this.trackCount = trackCount;
		}
		else {
			throw new IllegalArgumentException("Track Count must be greater than 0.");
		}
	}

	/**
	 * 
	 * @return format		Format of the Album
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * 
	 * @param format 	Format of the album
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	
	/**
	 * @precondition	obj is an Object of type Album
	 * @return 			True if albumNames are the same else false
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Album) {
			if (this.albumName.equalsIgnoreCase(((Album) obj).albumName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return A string representation of the album.
	 */
	public String toString() {
		return "---------------------------------\n" +
			   "Artist: " + artist + "\n" +
			   "Album Name: " + albumName + "\n" +
			   "Year: " + year + "\n" +
			   "Track Count: " + trackCount + "\n" +
			   "Format: " + format + "\n" + 
			   "---------------------------------\n";
	}
	
	/**
	 * Compare two albums based on alphabetical albumName field.
	 * @precondition	item is type Album.
	 * @param	item    Album type to be compared.
	 * @return	-1		If this.albumName < item.albumName.
	 * 			 0		If this.albumName == item.albumName.
	 * 			 1		If this.albumName > item.albumName.
	 */
	public int compareTo(Album item) {
		if (this.albumName.compareToIgnoreCase(item.albumName) < 0) {
			return -1;
		}
		else if (this.albumName.compareToIgnoreCase(item.albumName) == 0) {
			return 0;
		}
		else {
			return 1;
		}
	}
	
	/**
	 * Clones an Album item.
	 * @precondition	Must be an Album type.
	 * @return 			Cloned album.
	 */
	public Album clone() {
		Album a = null;
		try {
			a = (Album)super.clone();
		}
		catch (CloneNotSupportedException ex) {
			throw new RuntimeException("This class does not implement Cloneable.");
		}
		
		return a;
	}

}
