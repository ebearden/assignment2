import java.util.Iterator;

/**
 * Data Structure that holds and manages Album types.
 * 
 * @author Elvin Bearden
 * @version Created on: Jan 18, 2013	
 * @version Modified on: 2/3/13:
 * 			Added implementation of Iterable and a sort method.
 */

public class AlbumList implements Iterable<Album>{
	
	/* Invariants:
	 * --The Album array listOfAlbums will always be in consecutive order with no
	 * 	 null values between Album entries.
	 * --The int numberOfItems will always be greater than zero. At any given time
	 *   it will hold the current number of albums in the AlbumList
	 */
	
	private int numberOfItems;
	private Album[] listOfAlbums;
	
	/**
	 * Creates structure for holding Albums. 
	 * @precondition							size > 0
	 * @param 		size						Must be greater than zero
	 * @throws 		IllegalArgumentException 	If size is 0 or less.
	 */
	public AlbumList(int size){
		numberOfItems = 0;
		
		if (size > 0) {
			listOfAlbums = new Album[size];
		}
		else {
			throw new IllegalArgumentException("Size must be greater than 0.");
		}
	}
	
	/**
	 * Add an Album object to the list.
	 * @precondition		numberOfItems < size of the list.
	 * 						Must be type Album.
	 * @param album			Album object to add.
	 * @throws Exception	throws Exception if list is full.
	 */
	public void add(Album album) throws IllegalArgumentException{
		if (numberOfItems < listOfAlbums.length) {
			listOfAlbums[numberOfItems] = album;
			numberOfItems++;
		}
		else {
			throw new IllegalArgumentException("Album list is full.");
		}
	}
	
	/**
	 * Remove an album from the list.
	 * @precondition			Must be type Album.
	 * @param toRemove		Name of the album to remove.
	 */
	public void remove(Album toRemove) {
		for (int i = 0; i < listOfAlbums.length; i++) {
			if (toRemove.equals(listOfAlbums[i])) {
				listOfAlbums[i] = null;
				numberOfItems--;
				shift(i);
				break;
			}	
		}
	}
	
	/**
	 * Finds an Album in the list
	 * @precondition		toFind must be type Album.
	 * @param toFind		The name of the album to search for.
	 * @return				Matched Album object if found,
	 * 						else null.
	 */
	public Album search(Album toFind) {		
		for (int i = 0; i < listOfAlbums.length; i++) {
			if (toFind.equals(listOfAlbums[i])) {
				return listOfAlbums[i];
			}	
		}
		
		return null;
	}
	
	/**
	 * Clears all albums out of the list.
	 */
	public void clear() {
		for (int i = 0; i < listOfAlbums.length; i++) {
			listOfAlbums[i] = null;
			numberOfItems = 0;
		}
	}
	
	/**
	 * Shift the array to fill in a deleted Album.
	 * @precondition	listOfAlbums.length > 0
	 * @param index		Index of shift point.
	 */
	private void shift(int index) {
		for (int i = index; i < listOfAlbums.length - 1; i++) {
			listOfAlbums[i] = listOfAlbums[i + 1];
		}
	}
	
	/**
	 * @return 	A string representation of all albums in the list.
	 */
	@Override
	public String toString() {
		String albumString = "";
		for (int i = 0; i < numberOfItems; i++) {
				albumString +=  listOfAlbums[i].toString();
		}
		
		return albumString;
	}
	
	/**
	 * Counts how many elements are in the list.
	 * @return	The number of items in the list.
	 */
	public int count() {
		return numberOfItems;
	}
	
	/**
	 * Gets an element from the List.
	 * @precondition	The parameter n must be < listOfAlbums.length
	 * @param n			The index of the element.
	 * @return			The Album at the specified index.
	 */
	public Album getElementAt(int n) {
		if (n < listOfAlbums.length) {
			return listOfAlbums[n];
		}
		else
			throw new IllegalArgumentException("Index out of bounds.");
	}

	/**
	 * @precondition	Album class must implement Iterator.
	 * @return			Iterator for AlbumList.
	 */
	@Override
	public Iterator<Album> iterator() {
		return new AlbumListIterator(this);
	}
	
	/**
	 * Sort the items alphabetically by albumName;
	 * Uses Insertion sort
	 * Based on sort method from in class handout.
	 * @precondition	Album type must implement Comparable interface 
	 */
	public void sort() {
		int i, j;
		Album temp;
		
		for (i = 1; i < numberOfItems; i++) {
			
			temp = listOfAlbums[i];
			
			for (j = i - 1; j >= 0 && (temp.compareTo(listOfAlbums[j]) == 0 || temp.compareTo(listOfAlbums[j]) == -1); j--) {
				listOfAlbums[j + 1] = listOfAlbums[j];
			}
			
			listOfAlbums[j + 1] = temp;
			
		}
	}
	
	
}